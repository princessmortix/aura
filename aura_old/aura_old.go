package main

import (
"fmt"
"github.com/sqweek/dialog"
"github.com/faiface/beep/speaker"
"github.com/faiface/beep/mp3"
"github.com/faiface/beep"
"log"
"time"
"os"
"os/exec"
"runtime"
"net/http"
"io"
)

var clear map[string]func()

func init() {
	clear = make(map[string]func())
	clear["linux"] = func() {
		cmd := exec.Command("clear")
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
	clear["windows"] = func() {
		cmd := exec.Command("cmd", "/c", "cls")
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
}

func CallClear() {
	value, ok := clear[runtime.GOOS]
	if ok {
		value()
	} else {
		panic("Your platform is unsupported! Can't clear terminal screen :(")
	}
}
func DownloadFile(filepath string, url string) error {

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}

func main() {
dialog.Message("Make sure you are running this application on a Windows terminal, else you can't see this game").Title("Is this a terminal?").Info()

fmt.Println(`
              ,---------------------------,
              |  /---------------------\  |
              | |      This is an       | |
              | |     uninteractive     | |
              | |    venting "game"     | |
              | |       about my        | |
              | |       thoughts.       | |
              |  \_____________________/  |
              |___________________________|
            ,---\_____     []     _______/------,
          /         /______________\           /|
        /___________________________________ /  | ___
        |                                   |   |    )
        |  _ _ _                 [-------]  |   |   (
        |  o o o                 [-------]  |  /    _)_
        |__________________________________ |/     /  /
    /-------------------------------------/|      ( )/
  /-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/ /
/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/ /
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Loading data....`)
time.Sleep(7 * time.Second)
CallClear()
time.Sleep(3 * time.Second)
fmt.Print("She was.. ")
time.Sleep(2 * time.Second)
fmt.Print("She came here with her family.\n")
time.Sleep(3 * time.Second)
fmt.Print("I thought it was just like another visit from her... ")
time.Sleep(2 * time.Second)
fmt.Println("even after what I- my mom did, she still was ok with me...\n")
time.Sleep(2 * time.Second)
fmt.Print("I guess I wasn't right")
time.Sleep(3 * time.Second)
CallClear()
fmt.Println(`
 _________________
|# : Chapter 1 : #|
|  :           :  |
|  :  My words :  |
|  :           :  |
|  :___________:  |
|     _________   |
|    | __      | P|
|    ||  |     | M|
\____||__|_____|__|
`)
fileUrl := "https://freesound.org/data/previews/259/259293_2843367-lq.mp3"
	err := DownloadFile("fx.mp3", fileUrl)
	if err != nil {
		panic(err)
	}
f, err := os.Open("fx.mp3")
	if err != nil {
		log.Fatal(err)
	}

	streamer, format, err := mp3.Decode(f)
	if err != nil {
		log.Fatal(err)
	}
	defer streamer.Close()

	speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))

	done := make(chan bool)
	speaker.Play(beep.Seq(streamer, beep.Callback(func() {
		done <- true
	})))

	<-done
f.Close()
CallClear()
time.Sleep(3 * time.Second)
fmt.Println(`
     O         O
      \\     // 
       \\   //
        \\ // 
       /~~~~~\
,-------------------,
| ,---------------, |
| |~~    ~     ~~ | |
| |  ~    ~     ~ | |
| |     ~  ~    ~ | |
| |  ~       ~    | |
| |_______________| |
|___________________|
|___________________|
`)
time.Sleep(3 * time.Second)
CallClear()
fmt.Println(`
     O         O
      \\     // 
       \\   //
        \\ // 
       /~~~~~\
,----------------------,
| ,------------------, |
| | She is one of the| |
| |  best relatives  | |
| |i have.. why i did| |
| |why mom didnt shh | |
| |__________________| |
|______________________|
|______________________|
`)
time.Sleep(10 * time.Second)
CallClear()
time.Sleep(7 * time.Second)
e := os.Remove("fx.mp3")
    if e != nil {
        log.Fatal(e)
    }
dialog.Message(`The instruction at "0x745f2780" referenced memory at "0x00000000". The memory could not be "read."

Click on OK to terminate the program`).Title("aura.exe - Application Error").Error()
fmt.Println(`Thats all, for today.
Thanks to "Daniel C. Au" and "lc" for their ASCII arts.
Thanks to MrAuralization for the floppy sound.`)
}