# Aura

The Diary of Aura game.

# Downloads:
[Click here](https://gitlab.com/princessmortix/aura/-/releases)

Any released version is under GNU GPLv2 license.
